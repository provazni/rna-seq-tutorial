# Introduction
This is a repository for a RNAseq tutorial given in courses in EMBL Heidelberg.
The master branch of this repository is for courses held outside of EMBL, the EMBLCTL branch is for courses in-house.


Whole tutorial has three main parts:
- Introduction to linux / command line
- Quality control, mapping and read counting
- Statistical analysis of read counts in *R*

Main source of instructions is the `RNAseq_data_analysis.html` file and the [Rmarkdown](https://rmarkdown.rstudio.com/) document that was used to create the website:`RNAseq_data_analysis.rmd`

Usually, we start the practicals with the `RNAseq_data_analysis.html` and later on, when it is time to switch to R, we open the `RNAseq_data_analysis.rmd` in Rstudio.

--------------------------------
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Introduction](#introduction)
- [Prerequisities](#prerequisities)
	- [Input data](#input-data)
	- [Software](#software)
	- [R and R packages](#r-and-r-packages)
- [Usage](#usage)
    - [Download](#download)
	- [Tweaks in the code](#tweaks-in-the-code)
- [Additional resources](#additional-resources)
- [Authors](#authors)
- [License](#license)

<!-- /TOC -->

------------------------------------

# Prerequisities

## Input data
For the mapping section of the tutorial we provide subset of sequencing reads in `fastq` format. These can be found in the `reads` folder. Note that the files are compressed using `gzip`.

For the R analysis we use a file called `gene.count.tab` as an input. This file can be found in the root directory of the tutorial.

## Software
As mentioned above, ideally you would be running the tutorial on a linux machine (we use [CentOS](https://www.centos.org/) or [Ubuntu](https://www.ubuntu.com/) distributions).

For the quality control part, [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and [MultiQC](https://multiqc.info) are needed.

We mention [Cutadapt](http://cutadapt.readthedocs.io/en/stable/guide.html) in the trimming section, but it is not needed for the tutorial itself.

For mapping you will need a reference - we included mouse genome and chr2 reference in the `reference` folder.

The mapping itself is done with [STAR](https://github.com/alexdobin/STAR), but you will need also [samtools](http://www.htslib.org/doc/samtools.html) to work with the alignments.

Exploration of alignments needs [IGV](http://software.broadinstitute.org/software/igv/)

Read counting is done with [HTSeq-count](https://htseq.readthedocs.io/en/release_0.9.1/overview.html)

## R and R packages
Differential expression analysis is done using R statistical language.
You will need to download it and install it from: <https://cran.r-project.org/>

Since R is mainly command line based, we also highly recommend [Rstudio](https://www.rstudio.com/) that adds graphical user interface and makes a lot of things simpler.

For information about package installation in general, please visit:
 <https://www.r-bloggers.com/installing-r-packages/>

You will need following packages that are installed with R:
- `ggplot2`
- `gplots`
- `pheatmap`

you can do this in one step in R console by running this command:
`install.packages(c("ggplot2","gplots","pheatmap"))`

Then on top of that you need packages installed through [Bioconductor](https://www.bioconductor.org/)

- `DESeq2`
- `genefilter`
- `BiocParallel` (only needed for parallelization, you can skip it if you want)
- `org.Mm.eg.db`


# Usage

## Download
To go through the tutorial, you can clone the gitlab repository with git:

```git clone https://git.embl.de/provazni/rna-seq-tutorial.git```

Or you can just download and unzip the repository.
You then need to unpack the `index.tar.gz` file as a `index` folder.

## Tweaks in the code
In places where we reference any paths to files, you will need to change the code to reflect your own folder structre.
For example **/home/student/rna-seq-tutorial/** in the scripts needs to be changed to **/home/provaznik/practicals/rna-seq-tutorial/** in my case.

----------------------------
# Additional resources
[DESeq2 tutorial - Analyzing RNA-seq data with DESeq2](https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html)

# Authors
- Jonathan Landry: <landry@embl.de>
- Jan Provaznik: <provazni@embl.de>

Intro to R is adapted from practicals made by Jack Monahan and Yuvia Perez Rico [Link to original git](https://github.com/EnrightLab/Courses-and-Practicals/blob/master/EMBO_Heidelberg_2017/Intro_R/Intro_R_Practical.md)

# License
CC BY-NC-SA 4.0
